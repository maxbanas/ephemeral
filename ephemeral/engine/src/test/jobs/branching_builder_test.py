"""Tests a job with multiple branches in the workflow..
"""
import unittest
from ephemeral.engine.src.main.builder.job_builder import JobBuilder


class TestBranchingBuilderPipeline(unittest.TestCase):
    """
    Tests a job that has a workflow which branches into separate processing
    and output paths.
    """

    def test_branching(self):
        job = JobBuilder()

        @job.task('capitalize-words')
        async def capitalize_words(words_map):
            """
            A simple function to illustrate use of the task factory pattern.
            """
            words = words_map["strings"]
            capitalized_words = [word.upper() for word in words]
            return {"strings": capitalized_words}

        @job.task('filter-str')
        async def filter_string_length(strings_map, word_length=None, comparison=None):
            """
            Splits a string into a list and returns it.
            """
            string_list = strings_map["strings"]
            if comparison == "less":
                filtered_strings = [string for string in string_list if len(string) < word_length]
            elif comparison == "greater":
                filtered_strings = [string for string in string_list if len(string) > word_length]
            else:
                filtered_strings = string_list
            return {"strings": filtered_strings}

        @job.task('split-string')
        async def split_string(string_map):
            """
            Splits a string into a list and returns it.
            """
            input_string = string_map["input"]
            split = input_string.split()
            return {"strings": split}

        @job.task('word-count')
        async def char_count(words_map):
            """
            A simple function to illustrate use of the task factory pattern.
            """
            words = words_map["strings"]
            word_length = [len(word) for word in words]
            return {"lengths": word_length}

        # Define workflow
        # Start by creating an input
        job_input = job.input('An input string for testing splitting of words')

        # Pass the input to the split_string task
        split = split_string(job_input)

        # Pass the result of split_string to capitalize_words
        capitalized = capitalize_words(split)

        # Branch off...
        filtered_short = filter_string_length(capitalized, word_length=6, comparison='less')
        filtered_long = filter_string_length(capitalized, word_length=5, comparison='greater')

        wordcount_short = char_count(filtered_short)
        wordcount_long = char_count(filtered_long)

        # Use job.output to add an output function to the builder
        job.output(wordcount_short)
        job.output(wordcount_long)

        # Finally, run the job
        job_output = job.run()

        # Or use get_job_map() - the resulting job map should be in the same format as the
        # output from job_driver.build_job_from_map():
        # job_map = job.get_job_map()
        # Then submit the job using manual_controller.submit:
        # job_output = manual_controller.submit_job(job_map)


        expected_short = [2, 5, 3, 2, 5]
        expected_long = [6, 7, 9]

        # TODO re-write this part of test to use names instead of index once a better naming method for
        # builder tasks is figured out
        result_short = job_output[0]['data'][0]['lengths']
        result_long = job_output[1]['data'][0]['lengths']

        print('result_short={}, expected={}'.format(result_short, expected_short))
        print('result_long={}, expected={}'.format(result_long, expected_long))

        self.assertEqual(result_short, expected_short)
        self.assertEqual(result_long, expected_long)


if __name__ == '__main__':
    unittest.main()